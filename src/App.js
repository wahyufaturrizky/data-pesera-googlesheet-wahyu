/* eslint-disable react-hooks/exhaustive-deps */
import 'antd/dist/antd.css';
import './App.css';
import {
	DeleteOutlined,
	EditOutlined,
	FileAddOutlined,
	UnorderedListOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Row,
	Modal,
	Table,
	Space,
} from 'antd';
import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';

const { Content, Footer } = Layout;

const URL =
	'https://sheet.best/api/sheets/91d997d7-581f-49a7-9ea4-32f472ad0fc4';

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const App = () => {
	const [form] = Form.useForm();
	const [modal, contextHolder] = Modal.useModal();
	const [isLoading, setIsLoading] = useState(false);
	const [listData, setListData] = useState([]);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		dataId: null,
		isShowModalAction: false,
	});

	const { typeAction, dataRow, isShowModalAction, dataId } =
		isModalActionVisible;

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Age',
			dataIndex: 'age',
			key: 'age',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record, index) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									dataId: index,
									typeAction: 'VIEW',
								})
							}
							type='primary'
							icon={<UnorderedListOutlined />}>
							View
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									dataId: index,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									dataId: index,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const onSearch = (data) => {
		if (data === '') {
			fetchData();
		} else {
			let tempData = listData.filter(
				(filtering) => filtering.name.toLowerCase() === data.toLowerCase()
			);

			setListData(tempData);
		}
	};

	const fetchData = useCallback(() => {
		setIsLoading(true);
		axios
			.get(URL)
			.then((res) => {
				setListData(res.data);
				setIsLoading(false);
			})
			.catch((err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed input data!',
					content: <p>{err.response.data.detail || 'Internal server error'}</p>,
				});
			});
	}, []);

	useEffect(() => {
		fetchData();
	}, [fetchData]);

	const onFinish = (values) => {
		setIsLoading(true);

		axios
			.post(URL, values)
			.then((res) => {
				setIsLoading(false);
				modal.success({
					title: 'Success input data!',
					content: <p>New data success created</p>,
					onOk: () => {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
						fetchData();
					},
				});
				fetchData();
			})
			.catch((err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed input data!',
					content: <p>{err.response.data.detail || 'Internal server error'}</p>,
					onOk: () => {
						form.resetFields();
					},
				});
			});
	};

	const onDelete = async (values) => {
		setIsLoading(true);
		axios
			.delete(URL + `/${values}`)
			.then((res) => {
				setIsLoading(false);
				modal.success({
					title: 'Success delete data!',
					content: <p>New data success deleted</p>,
					onOk: () => {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
						fetchData();
					},
				});
				fetchData();
			})
			.catch((err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed delete data!',
					content: <p>{err.response.data.detail || 'Internal server error'}</p>,
					onOk: () => {
						form.resetFields();
					},
				});
			});
	};

	const onEdit = async (values) => {
		setIsLoading(true);
		axios
			.put(URL + `/${dataId}`, values)
			.then((res) => {
				setIsLoading(false);
				modal.success({
					title: 'Success update data!',
					content: <p>New data success updated</p>,
					onOk: () => {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
						fetchData();
					},
				});
				fetchData();
			})
			.catch((err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed updated data!',
					content: <p>{err.response.data.detail || 'Internal server error'}</p>,
					onOk: () => {
						form.resetFields();
					},
				});
			});
	};

	return (
		<Layout style={{ backgroundColor: 'white' }}>
			<Content>
				<Row justify='center' style={{ marginTop: 14 }}>
					<Col>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD',
										isShowModalAction: true,
									})
								}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								placeholder='wajib masukkan nama lengkap untuk material group desc.'
								onSearch={onSearch}
								allowClear
								oncle
								style={{ width: '40%' }}
							/>
						</Row>

						<Table
							loading={isLoading}
							bordered
							size='small'
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTable}
							dataSource={listData}
						/>
					</Col>
				</Row>

				<Modal
					title={typeAction}
					forceRender={false}
					visible={isShowModalAction}
					width={
						typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined
					}
					afterClose={() => form.resetFields()}
					onCancel={() => {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}}
					footer={
						typeAction === 'DELETE'
							? [
									<Button
										key='back'
										onClick={() => {
											form.resetFields();
											setIsModalActionVisible({
												...isModalActionVisible,
												isShowModalAction: false,
											});
										}}>
										Cancel
									</Button>,
									<Button
										loading={isLoading}
										onClick={() => onDelete(dataId)}
										type='danger'
										key='delete'>
										Iya, saya yakin
									</Button>,
							  ]
							: null
					}>
					{typeAction === 'VIEW' ? (
						dataRow &&
						Object.keys(dataRow).map((data, index) => {
							return (
								<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
									{data.replace('_', ' ')} :{' '}
									<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
								</p>
							);
						})
					) : typeAction === 'EDIT' ? (
						<Form
							{...formItemLayout}
							form={form}
							name='edit'
							onFinish={onEdit}
							fields={[
								{
									name: ['name'],
									value: dataRow.name,
								},
								{
									name: ['address'],
									value: dataRow.address,
								},
								{
									name: ['age'],
									value: dataRow.age,
								},
							]}
							scrollToFirstError>
							<Row>
								<Col lg={12} md={24}>
									<Form.Item
										label='Name'
										name='name'
										rules={[
											{
												required: true,
												message: 'Please input your name!',
											},
										]}>
										<Input />
									</Form.Item>
								</Col>
								<Col lg={12} md={24}>
									<Form.Item
										label='Address'
										name='address'
										rules={[
											{
												required: true,
												message: 'Please input your addess!',
											},
										]}>
										<Input />
									</Form.Item>
								</Col>
							</Row>

							<Row>
								<Col lg={12} md={24}>
									<Form.Item
										label='Age'
										name='age'
										rules={[
											{
												required: true,
												message: 'Please input your age!',
											},
										]}>
										<Input type='number' />
									</Form.Item>
								</Col>
							</Row>

							<Form.Item hidden name='guid' label='guid'>
								<Input />
							</Form.Item>
							<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
								<Form.Item>
									<Button loading={isLoading} type='primary' htmlType='submit'>
										Save
									</Button>
								</Form.Item>
							</div>
						</Form>
					) : typeAction === 'ADD' ? (
						<Form
							{...formItemLayout}
							form={form}
							name='addMasterSpec'
							onFinish={onFinish}
							scrollToFirstError>
							<Row>
								<Col lg={12} md={24}>
									<Form.Item
										label='Name'
										name='name'
										rules={[
											{
												required: true,
												message: 'Please input your name!',
											},
										]}>
										<Input />
									</Form.Item>
								</Col>
								<Col lg={12} md={24}>
									<Form.Item
										label='Address'
										name='address'
										rules={[
											{
												required: true,
												message: 'Please input your addess!',
											},
										]}>
										<Input />
									</Form.Item>
								</Col>
							</Row>

							<Row>
								<Col lg={12} md={24}>
									<Form.Item
										label='Age'
										name='age'
										rules={[
											{
												required: true,
												message: 'Please input your age!',
											},
										]}>
										<Input type='number' />
									</Form.Item>
								</Col>
							</Row>

							<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
								<Form.Item>
									<Button loading={isLoading} type='primary' htmlType='submit'>
										Create
									</Button>
								</Form.Item>
							</div>
						</Form>
					) : (
						<p>
							Apakah anda ingin menghapus daata{' '}
							<b>{dataRow?.material_group_description}</b> ini ?
						</p>
					)}
				</Modal>
				{contextHolder}
			</Content>
			<Footer>
				<Row justify='center'>
					<Col>
						<p>
							Create By{' '}
							<a
								rel='noreferrer'
								target='_blank'
								href='https://www.linkedin.com/in/wahyu-fatur-rizky/'>
								Wahyu Fatur Rizki
							</a>
						</p>
					</Col>
				</Row>
			</Footer>
		</Layout>
	);
};

export default App;
